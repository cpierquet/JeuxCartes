JeuxCartes is a package with playing cards.
--------------------------------------------------
JeuxCartes est un package avec des cartes à jouer.
--------------------------------------------------
Author : Cédric Pierquet
email : cpierquet@outlook.fr
Licence : Released under the LaTeX Project Public License v1.3c or later, see http://www.latex-project.org/lppl.txt
Poker v1 cards Image's Licence : LGPL-2.1 license https://github.com/htdebeer/SVG-cards
Poker v2 cards Image's Licence : Public Domain https://tekeye.uk/playing_cards/svg-playing-cards
Poker v3 cards Image's Licence : Public Domain https://www.me.uk/cards/
Tarot v1 cards Image's Licence : Public Domain https://freesvg.org/deck-of-french-tarot-playing-cards
Uno v1 cards Image's Licence   : MIT License https://eperezcosano.github.io/uno-part1/